import mnist
import numpy as np
import scipy.io
import os
from scipy.misc import imread, imresize
from torchvision import transforms
import torch




def mnist_to_svhn_format(X):
	X  = X.reshape(-1,28,28,1)
	X_RGB = np.concatenate([X, X, X], axis=3)
	X_conv = np.array([imresize(im, (32,32,3)) for im in X_RGB])
	X_conv = X_conv.transpose(0,3,1,2)
	#return X_conv.reshape(-1,3,32,32)
	return X_conv

def load_train_mnist():
	images = mnist.train_images()
	labels = mnist.train_labels()

	images = mnist_to_svhn_format(images)
	return images, labels

def load_test_mnist():
	images = mnist.test_images()
	labels = mnist.test_labels()

	images = mnist_to_svhn_format(images)
	return images, labels

def load_train_svhn():
	train_data = scipy.io.loadmat('svhn/train_32x32.mat')
	images = train_data['X']
	labels = train_data['y']
	images = images.transpose(3,2,0,1).astype(np.float32)
	
	labels = labels.reshape((-1,))
	labels[labels==10] = 0
	return images, labels

def load_test_svhn():
	test_data = scipy.io.loadmat('svhn/test_32x32.mat')
	images = test_data['X']
	labels = test_data['y']
	images = images.transpose(3,2,0,1).astype(np.float32)
	
	labels = labels.reshape((-1,))
	labels[labels==10] = 0 
	return images, labels

def normalize_dataset(X):
	X = X - np.mean(X, axis=0)
	X = X / np.std(X, axis=0)
	X[np.isnan(X)] = 0
	return X