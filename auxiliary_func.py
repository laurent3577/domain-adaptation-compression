import numpy as np 
import torch
import subprocess

def grbf(x1,x2,sigma=None, ret_sigma=False):
	n = x1.shape[0]
	m = x2.shape[0]
	x1 = torch.cuda.FloatTensor(x1)
	x2 = torch.cuda.FloatTensor(x2)
	if x1.dim() == 2:
		k1 = (x1*x1).sum(dim=1)
	else:
		k1 = x1*x1

	q = k1.repeat(m,1).t()
	del k1
	
	if x2.dim() == 2:
		k2 = (x2*x2).sum(dim=1)
	else:
		k2 = x2*x2
	r = k2.repeat(n,1)
	del k2
	
	h = q + r
	del q,r
	
	# The norm
	if x1.dim() == 2:
		h = h - 2*torch.matmul(x1,x2.t())
	else:
		h = h - 2 * torch.ger(x1, x2)
	h = h.clamp(min=0)
	sigma = np.sqrt(h.view(-1).median()/2) if not sigma else sigma 
	sigma = 1e-8 if sigma==0 else sigma
	if ret_sigma:
		return torch.exp(-1.*h/(2.*sigma**2)), sigma
	else :
		return torch.exp(-1.*h/(2.*sigma**2))


def MMD(X,Y,sigma=None):
	Nx, Ny = X.shape[0], Y.shape[0]
	Kxy, sigma = grbf(X, Y, sigma, ret_sigma=True)
	Kxx = grbf(X, X, sigma)
	Kyy = grbf(Y, Y, sigma)
	mmd = 1./Nx**2 * Kxx.sum() + 1./Ny**2 * Kyy.sum() - 1./(Nx*Ny) * Kxy.sum()
	return mmd

def show_gpu():
	output = subprocess.check_output(['nvidia-smi'])
	output_str = output.decode("utf-8").split("\n")
	for l in output_str:
		print(l)
	return