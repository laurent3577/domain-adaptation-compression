import mnist
import numpy as np
from DigitsModels import *
from DigitsDatasets import *
from MaximumDiscrepancyClassifier import MDC
import argparse
import json
import datetime

def get_date():
	dt = datetime.datetime.now()
	dt = dt.replace(hour = min(23,dt.hour + 9))
	return dt.isoformat()

if __name__ == '__main__':

	parser = argparse.ArgumentParser()
	parser.add_argument("--reg", help='regularisation to use (default is None)', type=str)
	parser.set_defaults(reg=False)
	parser.add_argument("-l", help='l parameter for regularisation in compression', type=float)
	parser.set_defaults(l=0.25)
	parser.add_argument('--no-fine-tuning', help='Compression will not use fine tuning', dest='fine_tuning', action='store_false')
	parser.set_defaults(fine_tuning=True)
	parser.add_argument('--with-source', help='Use source data for compression', dest='with_source', action='store_true')
	parser.set_defaults(with_source=False)
	args= parser.parse_args()

	reg = args.reg
	l = args.l
	fine_tuning = args.fine_tuning
	with_source = args.with_source

	############ Loading data

	print("Loading data....")

	Xs_train, ys_train = load_train_svhn()
	Xt_train, yt_train = load_train_mnist()

	Xt_test, yt_test = load_test_mnist()

	mdc = MDC(FeatureGenerator(), Classifier(), Classifier())

	try:
		results_file = json.load(open('Evaluate_comp.json', 'r'))
	except IOError:
		print('File not found creating new one')
		results_file = {}

	res_id = np.random.randint(0,1e8)

	results =  {
				"reg":reg,
				"l":l,
				"fine_tuning": fine_tuning,
				"with_source": with_source,
				"results":{},
				"date":""
				 }
	results_file[res_id] = results
	json.dump(results_file, open('Evaluate_comp.json', 'w'))
	print("Data loaded, loading model")
	mdc.FG.load_state_dict(torch.load('mdc_uncomp_FG.pth'))
	mdc.C.load_state_dict(torch.load('mdc_uncomp_C.pth'))
	mdc.C2.load_state_dict(torch.load('mdc_uncomp_C2.pth'))
	init_nb_params = mdc.FG.nb_params() + mdc.C.nb_params()
	fine_tuning_args = [Xs_train, ys_train, Xt_train, 128, 15, 400, 0.2, 2e-4, True]
	for alpha in 1 - np.arange(0.04, 0.152, 0.002):
		mdc = MDC(FeatureGenerator(), Classifier(), Classifier())
		mdc.FG.load_state_dict(torch.load('mdc_uncomp_FG.pth'))
		mdc.C.load_state_dict(torch.load('mdc_uncomp_C.pth'))
		mdc.C2.load_state_dict(torch.load('mdc_uncomp_C2.pth'))
		offset = np.random.randint(0, min(len(Xt_train)- 14000, len(Xs_train)-14000))
		target, source = Xt_train[offset:offset+14000], Xs_train[offset:offset+14000]
		labels = ys_train[offset:offset+14000]
		mdc.compress(target, source, alpha, l, fine_tuning_args, fine_tuning=fine_tuning, reg=reg, labels=labels, with_source=with_source)

		score = mdc.score(yt_test, mdc.predict(Xt_test))
		comp_rate = 1-round((mdc.FG.nb_params() + mdc.C.nb_params())/init_nb_params,3)
		print("Compressed score : ", score)
		print("Compression rate : ", comp_rate)
		results_file = json.load(open('Evaluate_comp.json', 'r'))

		results_file[str(res_id)]['results'][comp_rate] = score
		date = get_date()
		results_file[str(res_id)]['date'] = date
		json.dump(results_file, open('Evaluate_comp.json', 'w'))
		torch.cuda.empty_cache()

