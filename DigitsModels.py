import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torch.autograd import Function
from torch.autograd import Variable
from Compression import *
#torch.set_default_tensor_type('torch.cuda.FloatTensor')

class GradReverse(Function):
	def __init__(self, lambd):
		self.lambd = lambd

	def forward(self, x):
		return x.view_as(x)

	def backward(self, grad_output):
		return (grad_output * -self.lambd)


def grad_reverse(x, lambd=1.0):
	return GradReverse(lambd)(x)


class FeatureGeneratorSmall(nn.Module):
	def __init__(self):
		super(FeatureGeneratorSmall, self).__init__()
		self.conv1 = nn.Conv2d(3,32, kernel_size=3, stride=1)
		self.conv2 = nn.Conv2d(32,32, kernel_size=3, stride=1)
		self.conv3 = nn.Conv2d(32,64, kernel_size=3, stride=1)
		self.conv4 = nn.Conv2d(64,64, kernel_size=3, stride=1)
		self.conv5 = nn.Conv2d(64,128, kernel_size=3, stride=1)
		self.conv6 = nn.Conv2d(128,128, kernel_size=3, stride=1)

	def forward(self, x):
		x = F.relu(self.conv1(x))
		x = F.relu(self.conv2(x))
		x = F.max_pool2d(x,kernel_size=2,stride=1)
		x = F.relu(self.conv3(x))
		x = F.relu(self.conv4(x))
		x = F.max_pool2d(x,kernel_size=2,stride=1)
		x = F.relu(self.conv5(x))
		x = F.relu(self.conv6(x))
		x = F.max_pool2d(x,kernel_size=2,stride=1)
		
		features = x.view(-1, x.shape[1]*x.shape[2]*x.shape[3])
		return features



class FeatureGenerator(nn.Module):
	def __init__(self):
		super(FeatureGenerator, self).__init__()
		self.conv1 = nn.Conv2d(3, 64, kernel_size=5, stride=1, padding=2)
		self.batchnorm1 = nn.BatchNorm2d(64)
		self.conv2 = nn.Conv2d(64, 64, kernel_size=5, stride=1, padding=2)
		self.batchnorm2 = nn.BatchNorm2d(64)
		self.conv3 = nn.Conv2d(64, 128, kernel_size=5, stride=1, padding=2)
		self.batchnorm3 = nn.BatchNorm2d(128)
		self.dense1 = nn.Linear(128*8*8, 1024)
		self.batchnorm4 = nn.BatchNorm1d(1024)
		self.arch = [
						{
						'type': 'conv',
						'func': self.conv1,
						'name': 'Conv1'}, 
						{
						'type': 'bn',
						'func': self.batchnorm1,
						'name': 'BatchNorm1'}, 
						{
						'type': 'relu',
						'func': F.relu,
						'name': 'Relu1'},
						{
						'type': 'max_pool',
						'func': F.max_pool2d,
						'args': dict(kernel_size=3, stride=2, padding=1),
						'name': 'Max_pool1'},
						{
						'type': 'conv',
						'func': self.conv2,
						'name': 'Conv2'}, 
						{
						'type': 'bn',
						'func': self.batchnorm2,
						'name': 'BatchNorm2'}, 
						{
						'type': 'relu',
						'func': F.relu,
						'name': 'Relu2'},
						{
						'type': 'max_pool',
						'func': F.max_pool2d,
						'args': dict(kernel_size=3, stride=2, padding=1),
						'name': 'Max_pool2'},
						{
						'type': 'conv',
						'func': self.conv3,
						'name': 'Conv3'}, 
						{
						'type': 'bn',
						'func': self.batchnorm3,
						'name': 'BatchNorm3'}, 
						{
						'type': 'relu',
						'func': F.relu,
						'name': 'Relu3'},
						{
						'type': 'flatten',
						'args': self.batchnorm3,
						'name': 'Flatten1'},
						{
						'type': 'dense',
						'func': self.dense1,
						'name': 'Dense1'},
						{
						'type': 'bn',
						'func': self.batchnorm4,
						'name': 'BatchNorm4'},
						{
						'type': 'relu',
						'func': F.relu,
						'name': 'Relu4'}
						]

	def forward(self, x):
		x = F.relu(self.batchnorm1(self.conv1(x)))
		x = F.max_pool2d(x, kernel_size=3, stride=2, padding=1)
		x = F.relu(self.batchnorm2(self.conv2(x)))
		x = F.max_pool2d(x, kernel_size=3, stride=2, padding=1)
		x = F.relu(self.batchnorm3(self.conv3(x)))
		features = x.view(-1, x.shape[1]*8*8)
		features = F.relu(self.batchnorm4(self.dense1(features)))
		features = F.dropout(features, training=self.training)
		return features

	def total_nodes(self):
		nodes = 0
		for module in self.modules():
			if type(module) == torch.nn.modules.conv.Conv2d:
				nodes += module.out_channels
			elif type(module) == torch.nn.modules.linear.Linear:
				nodes += module.out_features
		return nodes

	def nb_params(self):
		total_params = 0
		for layer in self.children():
			for param in layer.parameters():
				total_params += np.prod(param.size())
		return total_params
	
class Classifier(nn.Module):
	def __init__(self):
		super(Classifier, self).__init__()
		self.clf2 = nn.Linear(1024, 1024)
		self.bn2 = nn.BatchNorm1d(1024)
		self.clf3 = nn.Linear(1024,10)
		self.arch = [
						{
						'type': 'dense',
						'func': self.clf2,
						'name': 'Dense2'},
						{
						'type': 'bn',
						'func': self.bn2,
						'name': 'BatchNorm5'},
						{
						'type': 'relu',
						'func': F.relu,
						'name': 'Relu5'},
						{
						'type': 'dense',
						'func': self.clf3,
						'name': 'Dense3'}
						]

	def forward(self, x, reverse=False):
		if reverse:
			x = grad_reverse(x)
		x = F.relu(self.bn2(self.clf2(x)))
		pred = self.clf3(x)
		return pred

	def total_nodes(self):
		nodes = 0
		for module in self.modules():
			if type(module) == torch.nn.modules.conv.Conv2d:
				nodes += module.out_channels
			elif type(module) == torch.nn.modules.linear.Linear:
				nodes += module.out_features
		return nodes

	def nb_params(self):
		total_params = 0
		for layer in self.children():
			for param in layer.parameters():
				total_params += np.prod(param.size())
		return total_params

class ClassifierSmall(nn.Module):
	def __init__(self):
		super(ClassifierSmall, self).__init__()
		self.fc1 = nn.Linear(36992,128)
		self.fc2 = nn.Linear(128,10)

	def forward(self,x):
		x = F.relu(self.fc1(x))
		pred = self.fc2(x)
		return pred

