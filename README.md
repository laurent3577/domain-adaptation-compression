The domain adaptation method used for training of the model is from the following paper https://arxiv.org/abs/1712.02560.

The compression method was also studied on an implementation of an AlexNet network, pretrained on the ImageNet dataset and fine tuned on the Oxford-102 flower dataset.