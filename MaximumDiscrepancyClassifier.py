import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import copy
from torch.autograd import Function
from torch.autograd import Variable
from Compression import *
from auxiliary_func import *

torch.set_default_tensor_type('torch.cuda.FloatTensor')

class Teacher(object):
	def __init__(self, FeatureGenerator, Classifier):
		self.FG = FeatureGenerator
		self.C = Classifier

class MDC(object):

	def __init__(self, FeatureGenerator, Classifier1, Classifier2):
		self.FG = FeatureGenerator
		self.C = Classifier1
		self.C2 = Classifier2
		self.FG = self.FG.cuda()
		self.C = self.C.cuda()
		self.C2 = self.C2.cuda()
		self.arch = self.FG.arch + self.C.arch
		del self.FG.arch
		del self.C.arch
		self.val_scores = []

	def reset_grad(self):
		self.opt_fg.zero_grad()
		self.opt_c.zero_grad()
		self.opt_c2.zero_grad()

	def discrepancy(self, out1, out2):
		return torch.mean(torch.abs(F.softmax(out1, dim=-1) - F.softmax(out2, dim=-1)))

	def shuffle(self, Xs, Ys, Xt):
		target_permut = np.random.permutation(Xt.shape[0])
		source_permut = np.random.permutation(Xs.shape[0])
		Xt = Xt[target_permut]
		Xs = Xs[source_permut]
		Ys = Ys[source_permut]

		return Xs, Ys, Xt


	def get_batches(self, Xs, Ys, Xt, batch_size):
		Xs, Ys, Xt = self.shuffle(Xs, Ys, Xt)
		batches = []
		for k in range(min(len(Xs), len(Xt))//batch_size):
			data = {
			'Xs': Xs[k*batch_size: (k+1)*batch_size],
			'Ys': Ys[k*batch_size: (k+1)*batch_size],
			'Xt': Xt[k*batch_size: (k+1)*batch_size],
			}
			batches.append(data)
		return batches


	def train(self, Xs, Ys, Xt, batch_size, nb_epoch, delay=400, alpha=0.2, lr=1e-3, verbose=True):
		self.opt_fg = optim.Adam(self.FG.parameters(), lr=lr, weight_decay=0.0005)
		self.opt_c = optim.Adam(self.C.parameters(), lr=lr, weight_decay=0.0005)
		self.opt_c2 = optim.Adam(self.C2.parameters(), lr=lr, weight_decay=0.0005)

		criterion = nn.CrossEntropyLoss()
		self.FG.train()
		self.C.train()
		self.C2.train()	
		step = 0
		for i in range(nb_epoch):
			batches = self.get_batches(Xs, Ys, Xt, batch_size)
			for batch_idx, data in enumerate(batches):
				source = data['Xs']
				source_labels = data['Ys']
				target = data['Xt']
				source = Variable(torch.cuda.FloatTensor(source))
				target = Variable(torch.cuda.FloatTensor(target))
				source_labels = Variable(torch.cuda.LongTensor(source_labels))
				self.reset_grad()
				source_features = self.FG(source)
				output_s1 = self.C(source_features)
				output_s2 = self.C2(source_features)
				loss_s1 = criterion(output_s1, source_labels)
				loss_s2 = criterion(output_s2, source_labels)
				loss_s = loss_s1 + loss_s2 
					


				if step < delay:
					loss = loss_s
					loss_dis = Variable(torch.cuda.FloatTensor([0]))
					loss_cent = Variable(torch.cuda.FloatTensor([0]))
				else:
					target_features = self.FG(target)
					output_t1 = self.C(target_features, reverse=True)
					output_t2 = self.C2(target_features, reverse=True)
					loss_dis = self.discrepancy(output_t1, output_t2)


					
				loss = (1-alpha)*loss_s - alpha*loss_dis
				
				loss.backward()
				
				self.opt_c.step()
				self.opt_c2.step()
				self.opt_fg.step()
				self.reset_grad()
				step += 1

			if verbose:
				print('Train Epoch: {} [{}/{} ({:.0f}%)]\tLoss1: {:.6f}\t Loss2: {:.6f}\t  Discrepancy: {:.6f}'.format(
							i+1, batch_idx+1, len(batches),
							100. * (batch_idx +1)/ len(batches), float(loss_s1.data), float(loss_s2.data), float(loss_dis.data)))
		return

	def xent_loss(self, p, q):
		return -torch.mean(torch.sum(p*torch.log(q+1e-8),1))

	def normalize(self, x):
		for k in range(x.shape[0]):
			x[k] /= x[k].norm()
		return x

	def perturb(self, x, p):
		pert = 1e-6 * self.normalize(torch.randn(x.shape))
		pert = Variable(pert, requires_grad=True)
		pert_probs = F.softmax(self.C(self.FG(x+pert)), dim=-1)
		loss = nn.KLDivLoss()(torch.log(pert_probs+1e-8), p.detach())

		loss.backward()

		perturb_direction = pert.grad 
		norm_perturb_direction = self.normalize(perturb_direction)
		return (x + norm_perturb_direction).detach()


	def dirt_t(self, Xt, batch_size, nb_epoch, gamma, beta, lr=1e-4, validation_data=None, verbose=True):
		opt_fg = optim.Adam(self.FG.parameters(), lr=lr, weight_decay=5e-4)
		opt_c = optim.Adam(self.C.parameters(), lr=lr, weight_decay=5e-4)
		self.teacher = Teacher(self.FG, self.C)
		opt_fg.zero_grad()
		opt_c.zero_grad()
		kld_loss = nn.KLDivLoss()
		eps = 1e-8
		init = True
		for i in range(nb_epoch):
			self.FG.train()
			self.C.train()
			batches = self.get_batches(Xt, Xt, Xt, batch_size)
			for batch_idx, batch in enumerate(batches):
				target = Variable(torch.cuda.FloatTensor(batch['Xt']))

				logits = self.C(self.FG(target))
				probs = F.softmax(logits, dim=-1)
				perturbs = Variable(self.perturb(target, probs).data, requires_grad=False)
				perturb_probs = F.softmax(self.C(self.FG(perturbs)), dim=-1)

				opt_fg.zero_grad()
				opt_c.zero_grad()

				cond_ent_loss = self.xent_loss(probs, probs) # enforce boundary to respect cluster assumption
				vat_loss = self.xent_loss(perturb_probs, probs.detach()) # enforce classifier to be locally lipchitz

				teacher_logits = self.teacher.C(self.teacher.FG(target)) if not init else 0
				teacher_probs = F.softmax(teacher_logits, dim=-1) if not init else 0
				stab_loss = kld_loss(torch.log(probs+eps), teacher_probs.detach()) if not init else Variable(torch.cuda.FloatTensor([0]))

				dirt_loss = gamma*(cond_ent_loss + vat_loss) + beta*stab_loss

				self.teacher.C = copy.deepcopy(self.C)
				self.teacher.FG = copy.deepcopy(self.FG)

				dirt_loss.backward()
				opt_fg.step()
				opt_c.step()
				opt_fg.zero_grad()
				opt_c.zero_grad()
				init = False

			if validation_data is not None:
				X_val = validation_data['X']
				y_val = validation_data['y']
				self.val_scores.append(self.score(y_val, self.predict(X_val)))
			if verbose:
				val_score = self.val_scores[-1] if validation_data is not None else 0
				print('Train Epoch: {} [{}/{} ({:.0f}%)] Val score : {}'.format(i+1, batch_idx+1, len(batches), 100. * (batch_idx +1)/ len(batches), val_score))
		return

	def output_layer_(self, x, layer_arch):
		if layer_arch['type'] in ['conv', 'bn', 'relu', 'dense']:
			return layer_arch['func'](x)
		elif layer_arch['type'] == 'max_pool':
			return layer_arch['func'](x, **layer_arch['args'])
		elif layer_arch['type'] == 'flatten':
			return x.view(-1, layer_arch['args'].num_features*8*8)

	def output_layer(self, x, i):
		for k in range(i+1):
			x = self.output_layer_(x, self.arch[k]).detach()
			torch.cuda.empty_cache()
		return x

	def compress_output(self, layer, J, layer_type):
		W = layer.weight.data
		if layer_type=='bn':
			W = W[J,]
		else:
			W = W[J,:]
		b = layer.bias.data
		b = b[J,]

		layer.weight = nn.Parameter(torch.cuda.FloatTensor(W))
		layer.bias = nn.Parameter(torch.cuda.FloatTensor(b))
		if layer_type == 'dense':
			layer.out_features = len(J)
		elif layer_type == 'conv':
			layer.out_channels = len(J)
		elif layer_type == 'bn':
			layer.running_mean = layer.running_mean[J,]
			layer.running_var = layer.running_var[J,]
			layer.num_features = len(J)

	def compress_input(self, layer, A_J, layer_type, conv_to_dense=False):
		W = layer.weight.data

		if layer_type == 'dense':
			W = W.cpu().numpy()
			out_dim = W.shape[0]
			if conv_to_dense:
				W = W.reshape(out_dim, A_J.shape[0], -1)
			W = np.moveaxis(W, (0,1), (-2,-1))
			W = np.matmul(W, A_J)
			W = np.moveaxis(W, (-2,-1), (0,1))
			W = W.ravel().reshape(W.shape)
			if conv_to_dense:
				W = W.reshape(out_dim, -1)
			layer.weight = nn.Parameter(torch.cuda.FloatTensor(W))
			layer.in_features = W.shape[1]

		elif layer_type == 'conv':
			W = W.cpu().numpy()
			W = np.matmul(W.transpose(2,3,0,1), A_J).transpose(2,3,0,1)
			layer.weight = nn.Parameter(torch.cuda.FloatTensor(W))
			layer.in_channels = W.shape[1]

	def compute_input_covar(self, target, source, i, with_source=True, return_inputs=False):
		"""Computes covariance matrix of the input to layer i"""
		print("Computing covariance matrix of layer...")
		if source is not None:
			X = Variable(torch.cuda.FloatTensor(np.vstack((target,source))), requires_grad=False).detach()
		else:
			X = Variable(torch.cuda.FloatTensor(target), requires_grad=False).detach()
		nb_target = target.shape[0]
		X = X.detach()
		torch.cuda.empty_cache()
		inputs = self.output_layer(X, i-1).data.cpu().numpy()
		if not with_source:
			cov_mat_data = inputs[:nb_target]
		else:
			cov_mat_data = inputs
		if self.arch[i]['type'] == 'dense':
			cov_mat =  1./cov_mat_data.shape[0] * np.matmul(cov_mat_data.T,cov_mat_data)
		else:
			nb_samples, nb_ch = cov_mat_data.shape[:2]
			cov_mat_data = cov_mat_data.reshape(nb_samples, nb_ch, -1)
			cov_mat = np.matmul(cov_mat_data, cov_mat_data.swapaxes(1,2)).mean(axis=0)
		print('Cov matrix computed as output of : ', self.arch[i-1]['name'], cov_mat.shape)
		torch.cuda.empty_cache()
		if return_inputs:	
			return cov_mat, inputs[:nb_target], inputs[nb_target:]
		return cov_mat, None, None

	def covar_mat(self, X):
		if X.ndim>2:
			nb_samples, nb_ch = X.shape[:2]
			X = X.reshape(nb_samples, nb_ch, -1)
			return np.matmul(X, X.swapaxes(1,2)).mean(axis=0)
		else:
			return 1./X.shape[0] * np.matmul(X.T, X)

	def compute_covar_diff(self, target_covar, source_covar, scale=False):
		covar_diff = []
		if scale:
			D = np.sqrt(1./(target_covar.diagonal()+1e-8))
			for i in range(source_covar.shape[0]):
				covar_diff.append(2*np.dot((source_covar[i,:] - target_covar[i,:])*D,(source_covar[i,:] - target_covar[i,:])*D[i]))
		else:
			for i in range(source_covar.shape[0]):
				covar_diff.append(2*np.linalg.norm(source_covar[i,:] - target_covar[i,:]))
		return np.array(covar_diff)

	def compute_mean_discrep(self,  target_outputs, source_outputs, gaussian_kernel=False):
		mean_discrep = []
		if gaussian_kernel:
			for i in range(source_outputs.shape[1]):
				if source_outputs.ndim > 2:
					mean_discrep.append(MMD(source_outputs[:,i].reshape(source_outputs.shape[0],-1), target_outputs[:,i].reshape(target_outputs.shape[0],-1)))
				else:
					mean_discrep.append(MMD(source_outputs[:,i], target_outputs[:,i]))
		else :
			mean_s, mean_t = source_outputs.mean(axis=0), target_outputs.mean(axis=0)
			for i in range(source_outputs.shape[1]):
				mean_discrep.append(np.linalg.norm(mean_s[i] - mean_t[i]))
		return np.array(mean_discrep)

	def compute_fischer_discriminant_ratio(self, source_outputs, labels):
		class_spread = []
		for i in range(source_outputs.shape[1]):
			X = source_outputs[:,i].reshape(source_outputs.shape[0],-1) if source_outputs.ndim > 2 else source_outputs[:,i]
			mu = np.array([np.average(X, axis=0, weights=(labels==k)) for k in np.unique(labels)])
			covars = [self.covar_mat(X[labels==k]).diagonal().sum() for k in np.unique(labels)] if source_outputs.ndim >2 else [np.var(X[labels==k]) for k in np.unique(labels)]
			n = mu.shape[0]	
			norms = (mu*mu).sum(axis=1) if mu.ndim == 2 else mu*mu
			a = np.tile(norms, (n,1))
			r = a + a.transpose() - 2*mu.dot(mu.transpose()) if mu.ndim == 2 else a + a.transpose() - 2*np.outer(mu,mu)
			r = r.clip(0)
			a = np.tile(covars, (n,1))
			s = a + a.transpose()
			s[s==0] = 1
			r = r / s
			class_spread.append(r.sum()/2)

		return np.array(class_spread)

	def compute_reg(self, target_outputs, source_outputs, labels=None):
		sigma_s, sigma_t = self.covar_mat(source_outputs), self.covar_mat(target_outputs)
		R = []
		covar_diff = self.compute_covar_diff(sigma_s, sigma_t, scale=True)
		mean_discrep = self.compute_mean_discrep(source_outputs, target_outputs, gaussian_kernel=False)
		class_spread = self.compute_fischer_discriminant_ratio(source_outputs, labels)

		R = mean_discrep + covar_diff - 0.2*class_spread
		return R / np.max(np.abs(R))

	def compress(self, target, source, alpha, l=0.25, fine_tuning_args=[], fine_tuning=True, reg=False, labels=None, with_source=True):
		self.FG.eval()
		self.C.eval()
		self.C2.eval()
		
		
		for i,layer in enumerate(self.arch):
			if layer['type'] in ['dense', 'conv']:
				print('Compressing layer : ', layer['name'])
				if i<len(self.arch)-1: # last layer cannot be compressed (rows)
					
					
					if layer['type'] == 'dense':
						output_cov_mat, target_outputs, source_outputs = self.compute_input_covar(target, source, i+3, with_source=with_source, return_inputs=reg)

						R = self.compute_reg(target_outputs, source_outputs, labels) if reg else None
						layer_to_adapt = self.arch[i+3]
						conv_to_dense = False
						
					else:
						if self.arch[i+4]['type'] == 'conv': # from conv layer to conv layer
							output_cov_mat, target_outputs, source_outputs = self.compute_input_covar(target, source, i+4, with_source=with_source, return_inputs=reg)
							
							R = self.compute_reg(target_outputs, source_outputs, labels) if reg else None
							layer_to_adapt = self.arch[i+4]
							conv_to_dense = False

						elif self.arch[i+4]['type'] == 'dense' : # from conv layer to dense layer
							output_cov_mat, target_outputs, source_outputs = self.compute_input_covar(target, source, i+3, with_source=with_source, return_inputs=reg)
							
							R = self.compute_reg(target_outputs, source_outputs, labels) if reg else None
							layer_to_adapt = self.arch[i+4]
							conv_to_dense = True

					J, A_J = find_optimal_J(output_cov_mat, alpha, l=l, reg=reg, R=R)
					# C = self.covar_mat(source_outputs) - self.covar_mat(target_outputs)
					# mu = source_outputs.mean(axis=0) - target_outputs.mean(axis=0)
					# J, A_J = find_optimal_J_V2(output_cov_mat, C, mu, alpha, l=l, reg=reg)
					del output_cov_mat
					del target_outputs, source_outputs
					
					# Compressing output of layer
					self.compress_output(layer['func'], J, layer['type'])
					print('Compression from {} to {} dimension'.format(A_J.shape[0], A_J.shape[1]))
					# Compressing input of next layer accordingly
					self.compress_input(layer_to_adapt['func'], A_J, layer_to_adapt['type'], conv_to_dense)
					print('Next layer ', layer_to_adapt['name'], ' input compressed')
					# Compression of C2 inputs when needed
					if layer['name'] == 'Dense1':
						self.compress_input(self.C2.clf2, A_J, 'dense')

			elif layer['type'] == 'bn':
				print('Compressing layer :', layer['name'])
				self.compress_output(layer['func'], J, layer['type'])
				print('Compression from {} to {} dimension\n'.format(A_J.shape[0], A_J.shape[1]))
				if fine_tuning:
					print('Fine tuning of parameters...')
					self.train(*fine_tuning_args)
					torch.cuda.empty_cache()
			else:
				print('Layer : ', layer['name'], 'no compression')
				continue
			torch.cuda.empty_cache()
		print('Model fully compressed')
		del A_J
		del J
		return

	def predict(self, X):
		self.FG.eval()
		self.C.eval()
		X = Variable(torch.cuda.FloatTensor(X), requires_grad=False).detach()
		with torch.no_grad():
			preds = np.argmax(F.softmax(self.C(self.FG(X)), dim=-1).detach().cpu().numpy(), axis=-1)
		torch.cuda.empty_cache()
		return preds

	def score(self, y_pred, y_true):
		return np.mean(y_pred == y_true)


