import torch
import torch.nn as nn 
import torch.utils.model_zoo as model_zoo
import torch.nn.functional as F
import torch.optim as optim
from torch.autograd import Function
from torch.autograd import Variable
import numpy as np
from torchvision import datasets, transforms
from tqdm import tqdm
from Compression import *
from auxiliary_func import show_gpu
from PIL import ImageFile
ImageFile.LOAD_TRUNCATED_IMAGES = True

class AlexNet(nn.Module):
	def __init__(self, num_classes=1000, pretrained=True):
		super(AlexNet, self).__init__()
		self.weights_url = 'https://download.pytorch.org/models/alexnet-owt-4df8aa71.pth'
		self.features = nn.Sequential(
							nn.Conv2d(3, 64, kernel_size=11, stride=4, padding=2),
							nn.ReLU(inplace=True),
							nn.MaxPool2d(kernel_size=3, stride=2),
							nn.Conv2d(64, 192, kernel_size=5, padding=2),
							nn.ReLU(inplace=True),
							nn.MaxPool2d(kernel_size=3, stride=2),
							nn.Conv2d(192, 384, kernel_size=3, padding=1),
							nn.ReLU(inplace=True),
							nn.Conv2d(384, 256, kernel_size=3, padding=1),
							nn.ReLU(inplace=True),
							nn.Conv2d(256, 256, kernel_size=3, padding=1),
							nn.ReLU(inplace=True),
							nn.MaxPool2d(kernel_size=3, stride=2),
						)
		self.classifier = nn.Sequential(
							nn.Dropout(),
							nn.Linear(256 * 6 * 6, 4096),
							nn.ReLU(inplace=True),
							nn.Dropout(),
							nn.Linear(4096, 4096),
							nn.ReLU(inplace=True),
							nn.Linear(4096, 1000),
						)
		self.features_layers_type = ['conv', 'relu', 'maxpool','conv', 'relu', 'maxpool','conv', 'relu', 'conv', 'relu', 'conv', 'relu', 'maxpool']
		self.classifier_layers_type = ['dropout', 'linear', 'relu', 'dropout', 'linear', 'relu', 'linear']
		if pretrained:
			self.load_state_dict(model_zoo.load_url(self.weights_url))
			if num_classes != 1000:
				last_layer_dropped = list(self.classifier.children())[:-1]
				new_last_layer = last_layer_dropped + [nn.Linear(4096,num_classes)]
				self.classifier = nn.Sequential(*new_last_layer)
		self.features.cuda()
		self.classifier.cuda()


	def forward(self, x):
		x = self.features(x)
		x = x.view(x.size(0), x.size(1) * 6 * 6)
		x = self.classifier(x)
		return x

	def train(self, lr, nb_epochs, batch_size):
		init_lr = lr
		opt_features = optim.SGD(self.features.parameters(), lr=lr, weight_decay=5e-4, momentum=0.9, nesterov=True) 
		#opt_features = optim.Adam(self.features.parameters(), lr=lr, weight_decay=5e-4)
		opt_classifier = optim.SGD(self.classifier.parameters(), lr=lr, weight_decay=5e-4, momentum=0.9, nesterov=True) 
		#opt_classifier = optim.Adam(self.classifier.parameters(), lr=lr, weight_decay=5e-4)
		scheduler_features = optim.lr_scheduler.StepLR(opt_features, step_size=300, gamma=0.1)
		scheduler_classifier = optim.lr_scheduler.StepLR(opt_classifier, step_size=300, gamma=0.1)
		train_dir = 'Train/'
		normalize = transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])

		train_dataset = datasets.ImageFolder(train_dir, transforms.Compose([transforms.Resize(256),
																			transforms.CenterCrop(224),
																			transforms.ToTensor(),
																			normalize]))
		train_loader = torch.utils.data.DataLoader(train_dataset, batch_size=batch_size, shuffle=True)

		loss = nn.CrossEntropyLoss().cuda()
		self.features.train()
		self.classifier.train()
		for epoch in range(1,nb_epochs+1):
			pbar = tqdm(train_loader)
			for Xb, yb in pbar:
				scheduler_features.step()
				scheduler_classifier.step()
				Xb = Variable(Xb).cuda()
				yb = Variable(yb).cuda()

				logits = self.forward(Xb)
				_, pred = logits.topk(1,1)
				pred = pred.t()
				correct = pred.eq(yb.view(1, -1).expand_as(pred))
				
				acc = correct.sum().item() / correct.shape[1]
				loss_batch = loss(logits, yb)
				loss_batch.backward()
				opt_features.step()
				opt_classifier.step()
				opt_features.zero_grad()
				opt_classifier.zero_grad()

				pbar.set_description('Train Epoch : {0}/{1} Loss : {2:.4f}   Acc : {3:.3f}'.format(epoch, nb_epochs, float(loss_batch.data), acc))
		torch.cuda.empty_cache()

	def test(self):
		test_dir = 'Test/'
		self.features.eval()
		self.classifier.eval()
		normalize = transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])

		test_dataset = datasets.ImageFolder(test_dir, transforms.Compose([transforms.Resize(256),
																			transforms.CenterCrop(224),
																			transforms.ToTensor(),
																			normalize]))
		test_loader = torch.utils.data.DataLoader(test_dataset, batch_size=256)
		val = 0
		count = 0
		pbar = tqdm(test_loader)
		for Xb, yb in pbar:
			with torch.no_grad():
				Xb = Variable(Xb.cuda())
				yb = Variable(yb.cuda())
				outputs = self.forward(Xb)
			_, preds = outputs.topk(1,1)

			preds = preds.t()
			correct = preds.eq(yb.view(1, -1).expand_as(preds))

			val += correct.detach().cpu().numpy().sum()
			count += correct.shape[1]
		print('Test accuracy : {0:.4f}'.format(val/count))
		torch.cuda.empty_cache()
		return val/count

	def load_weights(self, weight_file):
		self.features.load_state_dict(torch.load(weight_file+'FG.pth'))
		self.classifier.load_state_dict(torch.load(weight_file+'C.pth'))

	def save_weights(self, output_file):
		torch.save(self.features.state_dict(), output_file+'FG.pth')
		torch.save(self.classifier.state_dict(), output_file+'C.pth')

	def nb_params(self):
		total_params = 0
		for layer in self.children():
			for param in layer.parameters():
				total_params += np.prod(param.size())
		return total_params

	def get_fc7_outputs(self, image_dir, n_limit):
		""" 
		"""
		normalize = transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])
		dataset = datasets.ImageFolder(image_dir, transforms.Compose([transforms.Resize(256),
																		transforms.CenterCrop(224),
																		transforms.ToTensor(),
																		normalize]))
		image_loader = torch.utils.data.DataLoader(dataset, batch_size=100, shuffle=True)
		data = None

		classifier_layers = list(self.classifier.children())

		for X, y in image_loader:
			first_skip = data is None
			X = self.features(Variable(X.cuda(), requires_grad=False)).detach()
			X = X.view(X.size(0), -1)
			for layer in classifier_layers[:-1]:
				out = layer.forward(X).detach()
				X = out
				torch.cuda.empty_cache()
			data = torch.cat((data,X.data),0) if not first_skip else X.data
			print('Tensor data : {}/{} loaded'.format(data.shape[0],n_limit))
			if data.shape[0]>=n_limit:
				break
		return data.cuda()

	def get_output(self, X, module, layer_index):
		layers = list(module.children())
		X = Variable(X, requires_grad=False).detach()
		for k, layer in enumerate(layers[:layer_index+1]):
			out = layer.forward(X).detach()
			X = out
			torch.cuda.empty_cache()
		return out

	def get_covar_mat(self,X):
		if len(X.shape) >2:
			nb_samples, nb_ch = X.shape[:2]
			X = X.view(nb_samples, nb_ch, -1)
			return X.matmul(X.permute(0,2,1)).mean(dim=0)
		else:
			return 1./X.shape[0] * torch.matmul(X.t(),X)

	def compute_covar_diff(self, target_covar, source_covar, scale=False):
		covar_diff = []
		if scale:
			D = torch.sqrt(1./(target_covar.diag()+1e-8))
			for i in range(source_covar.shape[0]):
				covar_diff.append(2*torch.matmul((source_covar[i,:] - target_covar[i,:])*D,(source_covar[i,:] - target_covar[i,:])*D[i]))
		else:
			for i in range(source_covar.shape[0]):
				covar_diff.append(2*torch.norm(source_covar[i,:] - target_covar[i,:]))
		return np.array(covar_diff)

	def compute_mean_discrep(self,  target_outputs, source_outputs, gaussian_kernel=False):
		target_outputs = target_outputs.cpu().numpy()
		source_outputs = source_outputs.cpu().numpy()
		mean_discrep = []
		if gaussian_kernel:
			for i in range(source_outputs.shape[1]):
				if source_outputs.ndim > 2:
					mean_discrep.append(MMD(source_outputs[:,i].reshape(source_outputs.shape[0],-1), target_outputs[:,i].reshape(target_outputs.shape[0],-1)))
				else:
					mean_discrep.append(MMD(source_outputs[:,i], target_outputs[:,i]))
		else :
			mean_s, mean_t = source_outputs.mean(axis=0), target_outputs.mean(axis=0)
			for i in range(source_outputs.shape[1]):
				mean_discrep.append(np.linalg.norm(mean_s[i] - mean_t[i]))
		return np.array(mean_discrep)

	def compute_reg(self, target_outputs, source_outputs):
		sigma_s, sigma_t = self.get_covar_mat(source_outputs), self.get_covar_mat(target_outputs)
		R = []
		covar_diff = self.compute_covar_diff(sigma_s, sigma_t, scale=True)
		mean_discrep = self.compute_mean_discrep(source_outputs, target_outputs, gaussian_kernel=False)
		
		R = mean_discrep + covar_diff
		return R / np.max(R)

	def compress_output(self, layer, J, layer_type):
		W = layer.weight.data
		W = W[J,:]
		b = layer.bias.data
		b = b[J,]

		layer.weight = nn.Parameter(torch.cuda.FloatTensor(W))
		layer.bias = nn.Parameter(torch.cuda.FloatTensor(b))
		if layer_type == 'linear':
			layer.out_features = len(J)
		elif layer_type == 'conv':
			layer.out_channels = len(J)

	def compress_input(self, layer, A_J, layer_type, conv_to_dense=False):
		W = layer.weight.data
		if type(A_J) == torch.Tensor:
			A_J = A_J.cpu().numpy()
		if layer_type == 'linear':
			W = W.cpu().numpy()
			out_dim = W.shape[0]
			if conv_to_dense:
				W = W.reshape(out_dim, A_J.shape[0], -1)
			W = np.moveaxis(W, (0,1), (-2,-1))
			W = np.matmul(W, A_J)
			W = np.moveaxis(W, (-2,-1), (0,1))
			W = W.ravel().reshape(W.shape)
			if conv_to_dense:
				W = W.reshape(out_dim, -1)
			layer.weight = nn.Parameter(torch.cuda.FloatTensor(W))
			layer.in_features = W.shape[1]

		elif layer_type == 'conv':
			W = W.cpu().numpy()
			W = np.matmul(W.transpose(2,3,0,1), A_J).transpose(2,3,0,1)
			layer.weight = nn.Parameter(torch.cuda.FloatTensor(W))
			layer.in_channels = W.shape[1]

	def compress(self, n_target, n_source, alpha, l, reg, with_source, fine_tuning=False, fine_tuning_args=[]):
		target_dir = 'Train/'
		source_dir = 'Source/'
		target_out = self.get_fc7_outputs(target_dir, n_target)
		source_out = self.get_fc7_outputs(source_dir, n_source) if with_source or reg else None
		cov_mat_data = torch.cat((target_out, source_out),0) if with_source else target_out

		cov_mat = self.get_covar_mat(cov_mat_data)
		R = torch.cuda.FloatTensor(self.compute_reg(target_out, source_out)) if reg else None
		J, A_J = find_optimal_J_torch(cov_mat, alpha, l=l, reg=reg, R=R, filter_out=True)

		classifier_layers = list(self.classifier.children())
		layer, next_layer = classifier_layers[-3], classifier_layers[-1]
		self.compress_output(layer, J, 'linear')
		self.compress_input(next_layer, A_J, 'linear', False)
		if fine_tuning:
			self.train(*fine_tuning_args)
		

		return len(J)





if __name__ == '__main__':
	
	model = AlexNet(102)
	
	model.cuda()
	model.load_weights('AlexNetUncomp')
	model.compress(n_target=1000, n_source=1000, alpha=0.6, l=1, reg=True, with_source=False)
	model.test()

