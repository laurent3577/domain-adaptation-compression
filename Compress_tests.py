import mnist
import numpy as np
from DigitsModels import *
from DigitsDatasets import *
from MaximumDiscrepancyClassifier import MDC
import json
import time




############ Loading data

print("Loading data....")

Xs_train, ys_train = load_train_svhn()
Xt_train, yt_train = load_train_mnist()

Xt_test, yt_test = load_test_mnist()

mdc = MDC(FeatureGenerator(), Classifier(), Classifier())

re_train = False
s = time.time()
if re_train:
	print("Data loaded, beginning training")
	mdc.train(Xs_train, ys_train, Xt_train, 128, 32, delay=400, alpha=0.3, lr=2e-4, verbose=True)
	uncompr_score = mdc.score(yt_test, mdc.predict(Xt_test))
	init_nb_params = mdc.FG.nb_params() + mdc.C.nb_params()
	print('Target Test accuracy :')
	print(uncompr_score)
	save = input('Save new model ? (Y/n)   ')
	if save.lower() == 'y':
		torch.save(mdc.FG.state_dict(), 'mdc_uncomp_FG.pth')
		torch.save(mdc.C.state_dict(), 'mdc_uncomp_C.pth')
		torch.save(mdc.C2.state_dict(), 'mdc_uncomp_C2.pth')
else:
	print("Data loaded, loading model")
	mdc.FG.load_state_dict(torch.load('mdc_uncomp_FG.pth'))
	mdc.C.load_state_dict(torch.load('mdc_uncomp_C.pth'))
	mdc.C2.load_state_dict(torch.load('mdc_uncomp_C2.pth'))
	init_nb_params = mdc.FG.nb_params() + mdc.C.nb_params()
uncompr_score = mdc.score(yt_test, mdc.predict(Xt_test))
print('Target Test accuracy :')
print(uncompr_score)
labels_comp = ys_train[:14000]
mdc.compress(Xt_train[:14000], Xs_train[:14000], 0.92, l=1, fine_tuning_args=[Xs_train, ys_train, Xt_train, 128, 10, 400, 0.2, 2e-4, True], fine_tuning=False, reg=True, labels=labels_comp, with_source=False)
print("Compressed score : ", mdc.score(yt_test, mdc.predict(Xt_test)))
print("Compression rate : ", 1-round((mdc.FG.nb_params() + mdc.C.nb_params())/init_nb_params,3))
print("Time elapsed : ", time.time() - s, "(s)")