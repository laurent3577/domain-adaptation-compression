import numpy as np 
import torch
import torch.nn.functional as F
from torch.autograd import Variable
from operator import itemgetter
import time


def submat(M,I,J):
	I = np.array(I)
	return M[I.reshape(-1,1), J]

def sort(L):
	L.sort()
	return L

def update_cov_div(P,C,i,J):
	return np.sqrt(P**2+2*np.dot(C[i,J],C[i,J])+C[i,i]**2)

def update_mean_div(Q,mu,i):
	return np.sqrt(Q**2+(mu[i]*mu[i]).sum())


def find_optimal_J(cov_mat, alpha, theta=0, l=0.25, reg=False, R=None):
	eps_stab = 1e-8
	assert np.all(cov_mat.T == cov_mat), "Covariance matrix is not symmetric"

	F = range(cov_mat.shape[0])
	candidate_index = list(F)

	for i in F:
		if cov_mat[i,i] == 0:
			candidate_index.remove(i)

	print('Number candidate nodes/filters :', len(candidate_index))
	t = np.trace(cov_mat)
	J = []

	L_ = np.array([np.trace(1./(cov_mat[i,i]+eps_stab)*np.outer(cov_mat[:,i],cov_mat[i,:]))/t for i in candidate_index])
	L = L_ - l * np.std(L_) * R[candidate_index] if reg else L_

	L[np.isnan(L)] = -np.inf
	j = np.argmax(L)

	v = L_[j]
	j = candidate_index[j]
	
	print("Ratio of info captured : ", round(v, 3), "  Node selected : ", j, "  Nb of nodes : ", len(J))

	J = [j]
	candidate_index.remove(j)
	while v < alpha:
		res = []
		if candidate_index == []:
			print('No more nodes/filters to consider though info criteria not met')
			break
		
		L_ = []
		for ind in candidate_index:
			J_ = sort(J + [ind])

			val_ = np.trace(np.dot(submat(cov_mat, F, J_), np.dot(np.linalg.inv(submat(cov_mat, J_, J_)+eps_stab*np.identity(len(J_))), submat(cov_mat, J_, F))))/t
			L_.append(val_)

		L = L_ - l * np.std(L_) * R[candidate_index] if reg else L_
		res = list(zip(candidate_index, L, L_))
		j, val, v = max(res, key=itemgetter(1))
		

		J = sort(J + [j])
		candidate_index.remove(j)
		
		print("Ratio of info captured : ", round(v, 3), "  Node selected : ", j, "  Nb of nodes : ", len(J))
	A = submat(cov_mat, F, J)
	B = np.linalg.inv(submat(cov_mat, J, J)+eps_stab*np.identity(len(J)))
	if np.any(np.isinf(B)):
		print("Warning inf values found during A_J computation")
		B[np.isinf(B)] = np.sign(B[np.isinf(B)]) * 1e10
	A_J = np.dot(A,B)

	return np.array(J), A_J

def find_optimal_J_V2(cov_mat, C, mu, alpha, theta=0, l=0.25, reg=False):
	eps_stab = 1e-8
	assert np.all(cov_mat.T == cov_mat), "Covariance matrix is not symmetric"

	F = range(cov_mat.shape[0])
	candidate_index = list(F)

	for i in F:
		if cov_mat[i,i] == 0:
			candidate_index.remove(i)

	print('Number candidate nodes/filters :', len(candidate_index))
	t = np.trace(cov_mat)
	m = np.linalg.norm(C) + np.linalg.norm(mu)
	J = []

	L_ = np.array([np.trace(1./(cov_mat[i,i]+eps_stab)*np.outer(cov_mat[:,i],cov_mat[i,:]))/t for i in candidate_index])
	reg_terms = np.array([(C[i,i] + np.sqrt((mu[i]*mu[i]).sum())) for i in candidate_index])
	reg_terms =reg_terms / np.max(reg_terms)
	L = L_ - l * np.std(L_) * reg_terms if reg else L_

	L[np.isnan(L)] = -np.inf
	j = np.argmax(L)

	v = L_[j]
	j = candidate_index[j]
	P = C[j,j]
	Q = np.sqrt((mu[j]*mu[j]).sum())
	print("Ratio of info captured : ", round(v, 3), "  Node selected : ", j, "  Nb of nodes : ", len(J))

	J = [j]
	candidate_index.remove(j)
	while v < alpha:
		res = []
		if candidate_index == []:
			print('No more nodes/filters to consider though info criteria not met')
			break
		
		L_ = []
		reg_terms = []		
		for s, ind in enumerate(candidate_index):			
			J_ = sort(J + [ind])

			val_ = np.trace(np.dot(submat(cov_mat, F, J_), np.dot(np.linalg.inv(submat(cov_mat, J_, J_)+eps_stab*np.identity(len(J_))), submat(cov_mat, J_, F))))/t
			L_.append(val_)
			reg_terms.append(update_cov_div(P,C,ind,J) + update_mean_div(Q,mu,ind))
		reg_terms =reg_terms / np.max(reg_terms)
		L = L_ - l * np.std(L_)*reg_terms if reg else L_
		res = list(zip(candidate_index, L, L_))
		j, val, v = max(res, key=itemgetter(1))
		
		P = update_cov_div(P,C,j,J)
		Q = update_mean_div(Q,mu,j)
		J = sort(J + [j])
		candidate_index.remove(j)
		
		print("Ratio of info captured : ", round(v, 3), "  Node selected : ", j, "  Nb of nodes : ", len(J))
	A = submat(cov_mat, F, J)
	B = np.linalg.inv(submat(cov_mat, J, J)+eps_stab*np.identity(len(J)))
	if np.any(np.isinf(B)):
		print("Warning inf values found during A_J computation")
		B[np.isinf(B)] = np.sign(B[np.isinf(B)]) * 1e10
	A_J = np.dot(A,B)

	return np.array(J), A_J

def update_inv_torch(A, S, i, J):
	eps_stab = 1e-8
	J_ = sort(J+[i])
	if len(J)==1:
		u = S[i,J[0]]
		d= 1./(S[i,i] - A*u**2 + eps_stab)
		w = -d*A*u 
		W = A + d*A**2*u**2
		A_updated = torch.cuda.FloatTensor([[W,w],[w,d]])
		perm = list(range(len(J_)))
		s = J_.index(i)
		perm.insert(s, perm[-1])
		perm.pop()
		A_updated = A_updated[:,perm][perm,:]
		return A_updated
	else:
		u = S[i][torch.cuda.LongTensor(J)]
		d = 1./(S[i,i] - u.matmul(A.matmul(u)) + eps_stab)
		w = - d*A.matmul(u)
		W = A + d*A.matmul(torch.ger(u,u).matmul(A))
		block = torch.cat((W,w.unsqueeze(0)),0)
		last_column = torch.cat((w,torch.cuda.FloatTensor([d])),0)
		A_updated = torch.cat((block, last_column.unsqueeze(1)),1)
		perm = list(range(len(J_)))
		s = J_.index(i)
		perm.insert(s, perm[-1])
		perm.pop()
		A_updated = A_updated[:,perm][perm,:]
		return A_updated

def find_optimal_J_torch(cov_mat, alpha, theta=0, l=0.25, reg=False, R=None, filter_out=False):
	eps_stab = 1e-8
	F = range(cov_mat.size(0))
	candidate_index = list(F)
	
	for i in F:
		if cov_mat[i,i] == 0:
			candidate_index.remove(i)
	print('Number candidate nodes/filters :', len(candidate_index))
	t = cov_mat.trace()

	L_ = torch.cuda.FloatTensor([(1./(cov_mat[i,i]+eps_stab)*torch.ger(cov_mat[:,i],cov_mat[i,:])).trace()/t for i in candidate_index])
	if reg:
		L = L_ - l * L_.std() * R[candidate_index]
		
	else:
		L = L_
	j = L.cpu().numpy().argmax()
	v = L_[j]
	j = candidate_index[j]

	J = [j]
	candidate_index.remove(j)

	print('Ratio of info captured : {}, Node selected : {}, Nb of nodes : {}'.format(round(v,3), j, len(J)))
	C_JJ_inv = 1./cov_mat[j,j]
	while v < alpha:
		res = []
		assert candidate_index != [], 'No remaining node/filter to add'
		L_ = []
		start = time.time()
		for ind in candidate_index:
			J_ = sort(J + [ind])

			C_FJ = submat(cov_mat, F, J_)
			C_JF = submat(cov_mat, J_, F)
			C_JJ_inv__ = update_inv_torch(C_JJ_inv, cov_mat, ind, J)
			#C_JJ_inv_ = (submat(cov_mat, J_, J_)+eps_stab*torch.eye(len(J_)).cuda()).inverse()
			val = (C_FJ.matmul(C_JJ_inv__.matmul(C_JF)).trace()/t)
			L_.append(val)
			res.append((ind, val, val))

		if reg:
			L = L_ - l * np.std(L_) * R[candidate_index].cpu().numpy()
			res = list(zip(candidate_index, L, L_))
		res.sort(key=itemgetter(1), reverse=True)
		
		j, val, v = res[0]
		end = time.time()
		J = sort(J + [j])
		if filter_out:
			to_remove, _, _ = res[-1]
			candidate_index.remove(to_remove)
		candidate_index.remove(j)
		C_JJ_inv = (submat(cov_mat, J, J)+eps_stab*torch.eye(len(J)).cuda()).inverse()
		
		print('Ratio of info captured : {}, Node selected : {}, Nb of nodes : {}, Time elapsed : {}'.format(round(v,3), j, len(J), end-start))


	A = submat(cov_mat, F, J)
	B = (submat(cov_mat, J, J) + eps_stab* torch.eye(len(J)).cuda()).inverse()
	A_J = A.matmul(B)

	return np.array(J), A_J